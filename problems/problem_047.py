# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)
#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    lowercase_letter = 1
    uppercase_letter = 1
    digit = 1
    special_char = 1
    min_char = 6
    max_char = 12

    for character in len(password):
        if character.isdigit:
            digit -= 1
            min_char -= 1
            max_char -= 1
        elif character.isalpha and character.isupper:
            uppercase_letter -= 1
            min_char -= 1
            max_char -= 1
        elif character.isalpha and character.islower:
            lowercase_letter -= 1
            min_char -= 1
            max_char -= 1
        else:
            special_char -= 1
            min_char -= 1
            max_char -= 1

    if lowercase_letter <= 0 and uppercase_letter <= 0 and digit <= 0 and special_char <=0 and min_char <=0 and max_char > 0:
        return True
    else:
        return False
