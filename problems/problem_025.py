# Complete the calculate_sum function which accepts
# a list of numerical values and returns the sum of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#

def calculate_sum(values):
    num_values = len(values)
    sum_values = 0
    if num_values == 0:
        return None
    else:
        for item in values:
            sum_values += item
    return sum_values
