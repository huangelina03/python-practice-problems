# Complete the has_quorum function to test to see if the number
# of the people in the attendees list is greater than or equal
# to 50% of the number of people in the members list.

def has_quorum(attendees_list, members_list):
    attendees_list_length = len(attendees_list)
    members_list_length = len(members_list)
    half_of_members = members_list_length/2

    if attendees_list_length >= half_of_members:
        return True
    else:
        return False
