# Write a function that meets these requirements.
#
# Name:       generate_lottery_numbers
# Parameters: none
# Returns:    a list of six random unique numbers
#             between 1 and 40, inclusive
#
# Example bad results:
#    [4, 2, 3, 3, 1, 5] duplicate numbers
#    [1, 2, 3, 4, 5] not six numbers
#
# You can use randint from random, here, or any of
# the other applicable functions from the random
# package.
#
# https://docs.python.org/3/library/random.html

import random

def generate_lottery_numbers():
    numbers = []
    numbers_total = 6
    for generated_num in range(numbers_total):
        new_num = random.randint(1, 40)
        if new_num not in numbers:
            numbers.append(new_num)
        else:
            generated_num -= 1
    print(numbers)
    return numbers

generate_lottery_numbers()
