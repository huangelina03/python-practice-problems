# Complete the is_palindrome function to check if the value in
# the word parameter is the same backward and forward.
#
# For example, the word "racecar" is a palindrome because, if
# you write it backwards, it's the same word.

# It uses the built-in function reversed and the join method
# for string objects.

# Do some planning in ./planning.md

# Write out some pseudocode before trying to solve the
# problem to get a good feel for how to solve it.

def is_palindrome(word):
    # word_length = len(word)
    # for char in word:
    #     if char != word(word_length-char):
    #         return False
    # return True

    reversed_word = reversed(word)
    reversed_word = "".join(reversed_word)
    if str(reversed_word) == word:
        return True
    else:
        return False

print(is_palindrome("level"))
