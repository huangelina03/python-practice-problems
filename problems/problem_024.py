# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    length_of_list = len(values)
    sum_values = 0
    if length_of_list is 0:
        return None
    else:
        for item in values:
            sum_values += item
    return sum_values / length_of_list
