# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(single_list):
    first_half = len(single_list) // 2
    first_list = []
    second_list = []
    for element in range(first_half):
        first_list.append(element)
    for item in range(len(single_list) // 2):
        second_list.append(item + first_half + 1)
    print(first_list)
    print(second_list)
    return(first_list, second_list)

single_list = [1, 2, 3, 4]
halve_the_list(single_list)
