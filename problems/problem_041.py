# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.

def add_csv_lines(csv_lines):
    new_list = []
    if len(csv_lines) == 0:
        return new_list
    for part in csv_lines:
        num = part.split(",")
        new_number = 0
        for number in num:
            new_number += int(number)
        new_list.append(new_number)
    print(new_list)
    return new_list

csv_list = "3", "1,9"
add_csv_lines(csv_list)
