# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    num_values = len(values)
    max_num = values[0]
    if num_values == 0:
        return None
    for number in values:
        if number > max_num:
            max_num = number
    return max_num
